<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        li{
            margin-bottom: 16px;
        }
        i{
            color:aqua;
            margin-right:10px
        }
        a{
            color:white;
            cursor: pointer;
            text-decoration: none;
        }
       img{
           width: 20%;
           height: 20%;
           margin-right: 5px;
           border-radius:50% ;
       }
    </style>
</head>
<body>
<div class="wrapper row">
        <div class="col-2 bg-dark header" style="height:800px">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <div class="column">
                    <img src="tour.jpg" class=" img-circle">
                    <span class="text-white" style="margin-top:30px">Admin</span>
                    <ul type="none" style="margin-top:50px" class="text-white">
                        <li> <i class="fa fa-home"></i> <a  style=" text-decoration: none;" href="esas">Əsas</a></li>
                        <li><i class="fa fa-envelope"></i> <a style=" text-decoration: none;" href="mesaj">Mesajlar</a></li>
                        <li><i class="fa fa-list"></i> <a style=" text-decoration: none;" href="tur">Turlar</a></li>
                        <li><i class="fa fa-users"></i> <a style=" text-decoration: none;" href="worker">İşçilər</a></li>
                        <li><i class="fa fa-car"></i> <a style=" text-decoration: none;" href="car">Maşınlar</a></li>
                        <li><i class="fa fa-pencil"></i> <a style=" text-decoration: none;" href="msk">MSK</a></li>
                    </ul>
                </div>
            </nav>
         </div>
    @yield('here')
</div>
</body>
</html>
