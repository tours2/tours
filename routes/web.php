<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','admin\AdminController@start');
Route::get('esas','admin\AdminController@esas');
Route::get('tur','admin\AdminController@tur');
Route::get('worker','admin\AdminController@work');
Route::get('msk','admin\AdminController@msk');
Route::get('mesaj','admin\AdminController@mesaj');
Route::get('car','admin\AdminController@car');

